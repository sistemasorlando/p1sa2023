import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { MarvelResponse } from '../interfaces/marvel-response';

@Injectable({
  providedIn: 'root'
})
export class ServiciosService {

  constructor( private http: HttpClient ) { }

  getMarvelCharacters(offset: string):Observable<MarvelResponse>{
    return this.http.get<MarvelResponse>('https://gateway.marvel.com/v1/public/characters?limit=12&offset='+offset+'&ts=1&apikey=89b15b6ee73ff84d5debfcf5fa611172&hash=af8803a158337c5fabcde144f08d579b')
  }
}
