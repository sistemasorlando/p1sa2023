import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RestComponent } from './pages/rest/rest.component';
import { SoapComponent } from './pages/soap/soap.component';


const routes: Routes = [
  {
    path: 'rest',
    component: RestComponent
  },
  {
    path: 'soap',
    component: SoapComponent
  },
  {
    path: '**',
    redirectTo: '/rest'
  }
];


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot( routes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule { }
