import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RestComponent } from './rest/rest.component';
import { SoapComponent } from './soap/soap.component';
import { ComponentsModule } from '../components/components.module';



@NgModule({
  declarations: [
    RestComponent,
    SoapComponent
  ],
  imports: [
    CommonModule,
    ComponentsModule
  ]
})
export class PagesModule { }
