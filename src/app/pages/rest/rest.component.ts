import { Component, OnInit } from '@angular/core';
import { Result } from 'src/app/interfaces/marvel-response';
import { ServiciosService } from 'src/app/services/servicios.service';

@Component({
  selector: 'app-rest',
  templateUrl: './rest.component.html',
  styleUrls: ['./rest.component.css']
})
export class RestComponent implements OnInit{

  constructor( private servicio:ServiciosService){ }

  public listado: Result[] = [];

  public offset: number = 0;
  
  ngOnInit(): void {

    this.servicio.getMarvelCharacters(this.offset+"")
    .subscribe( resp => {
      this.listado = resp.data.results;
      
    });
  }

  siguiente(){
    this.offset = this.offset+25;
    this.servicio.getMarvelCharacters(this.offset+"")
    .subscribe( resp => {
      this.listado = resp.data.results;
        
    }); 

  }


}
