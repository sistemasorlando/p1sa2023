import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-soap',
  templateUrl: './soap.component.html',
  styleUrls: ['./soap.component.css']
})
export class SoapComponent implements OnInit{

  public listado: any;
  public fecha: string = "";
  public referencia : string = "";

  ngOnInit(): void {

      var myHeaders = new Headers();
      myHeaders.append("Content-Type", "text/xml");
      var raw = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ws=\"http://www.banguat.gob.gt/variables/ws/\">\r\n   <soap:Header/>\r\n   <soap:Body>\r\n      <ws:VariablesDisponibles/>\r\n   </soap:Body>\r\n</soap:Envelope>";

      var raw2 = "<soap:Envelope xmlns:soap=\"http://www.w3.org/2003/05/soap-envelope\" xmlns:ws=\"http://www.banguat.gob.gt/variables/ws/\">\r\n   <soap:Header/>\r\n   <soap:Body>\r\n      <ws:TipoCambioDia/>\r\n   </soap:Body>\r\n</soap:Envelope>";

    fetch("https://www.banguat.gob.gt/variables/ws/TipoCambio.asmx", {
      method: 'POST',
      headers: myHeaders,
      body: raw,
      redirect: 'follow'
    })
    .then(response => response.text())
    .then(result => { 

    const jsonObject = this.convertirXMLaJSON(result);

    Object.entries(jsonObject).forEach(([key, value]) => {
      if(key == 'soap:Body'){
        var valores : any = value;
        this.listado = valores[0].VariablesDisponiblesResponse[0].VariablesDisponiblesResult[0].Variables[0].Variable
        
      }      
    });  
  })
  .catch(error => console.log('error', error));


  fetch("https://www.banguat.gob.gt/variables/ws/TipoCambio.asmx", {
    method: 'POST',
    headers: myHeaders,
    body: raw2,
    redirect: 'follow'
  })
  .then(response => response.text())
  .then(result => {

    const jsonObject = this.convertirXMLaJSON(result);

    Object.entries(jsonObject).forEach(([key, value]) => {
      if(key == 'soap:Body'){
        var valores : any = value;
        this.fecha = valores[0].TipoCambioDiaResponse[0].TipoCambioDiaResult[0].CambioDolar[0].VarDolar[0].fecha
        this.referencia = valores[0].TipoCambioDiaResponse[0].TipoCambioDiaResult[0].CambioDolar[0].VarDolar[0].referencia
        
      }      
    }); 
  })
  .catch(error => console.log('error', error));


  }

  convertirXMLaJSON(xmlString: string): any {
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(xmlString, "text/xml");
  
    // Función auxiliar para convertir un elemento XML en objeto JSON
    function xmlToJson(xml: any): any {
      const result: any = {};
  
      if (xml.hasAttributes()) {
        for (let i = 0; i < xml.attributes.length; i++) {
          const attribute = xml.attributes[i];
          result["@" + attribute.name] = attribute.value;
        }
      }
  
      if (xml.hasChildNodes()) {
        for (let i = 0; i < xml.childNodes.length; i++) {
          const child = xml.childNodes[i];
          if (child.nodeType === Node.ELEMENT_NODE) {
            if (child.childNodes.length === 1 && child.firstChild?.nodeType === Node.TEXT_NODE) {
              result[child.nodeName] = child.firstChild.nodeValue;
            } else {
              if (!result[child.nodeName]) {
                result[child.nodeName] = [];
              }
              result[child.nodeName].push(xmlToJson(child));
            }
          }
        }
      }
  
      return result;
    }
  
    return xmlToJson(xmlDoc.documentElement);
  }

}
